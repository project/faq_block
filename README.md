FAQ Block
===============

FAQ Block : FAQ Block provices the backend configuration block to add the list of commonly asked questions and answers about a specific topic.
We can place the block from structure - block layout or from the layout builder.

[Issue Tracker](https://www.drupal.org/project/issues/faq_block?version=1.x)

## Installation

Use [Composer](https://getcomposer.org/) to get FAQ + Block with all dependencies.

```
composer create-project faq_block/project-base mysite --stability dev --no-interaction
```
